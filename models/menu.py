# -*- coding: utf-8 -*- 

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.title = request.application
response.subtitle = T('presso il GLCA l\'Erba di Casatenovo')

##########################################
## this is the main application menu
## add/remove items as required
##########################################

response.menu = [
##    (T('Index'), False, URL(request.application,'default','index'), [])
    [T('Libri'), False, False, [
        [T('Lista completa'), False, URL(c='book', r=request, f='list', args=['show', '0']), []],
        [T('Lista dei prestiti'), False, URL(c='book', r=request, f='list', args=['show', '1', '1']), []],
        [T('Lista libri disponibili'), False, URL(c='book', r=request, f='list', args=['show', '1', '0']), []],
       # [T('Lista libri eliminati'), False, URL(c='book', r=request, f='list', args=['show', '4']), []],
       # [T('Lista libri per eliminarli'), False, URL(c='book', r=request, f='list_to_discard', args=['show', '3']), []],
        [T('Lista libri in vendita'), False, URL(c='book', r=request, f='list', args=['show', '3']), []],
        [T('Aggiungi libro'), False, URL(c='book', r=request, f='add',), []],
        [T('Aggiungi libro in vendita'), False, URL(c='book', r=request, f='add_in_vendita',), []],
        ]],
         
    [T('Video'), False, False, [
        [T('Lista completa'), False, URL(c='video', r=request, f='list', args=['show', '0']), []],
        [T('Lista dei prestiti'), False, URL(c='video', r=request, f='list', args=['show', '1', '1']), []],
        [T('Lista video disponibili'), False, URL(c='video', r=request, f='list', args=['show', '1', '0']), []],
        [T('Aggiungi video'), False, URL(c='video', r=request, f='add'), []],
        ]],

#   [T('Ebooks'), False, False, [
#       [T('Lista completa ordinata per autore'), False, URL(c='ebooks', r=request, f='ebooks', args='0'), []],
#       [T('Lista completa ordinata per titolo'), False, URL(c='ebooks', r=request, f='ebooks', args='1'), []],
#       [T('Lista completa ordinata per argomento'), False, URL(c='ebooks', r=request, f='ebooks', args='2'), []],
#       ]],

  # [T('Quick search'), False, URL(c='default', r=request, f='quick_search'), []],
    [T('Cerca'), False, False, [
        [T('Video'), False, URL(c='video', r=request, f='search'), []],
        [T('Libri'), False, URL(c='book', r=request, f='search'), []],
    ]],

    ]

##########################################
## this is here to provide shortcuts
## during development. remove in production 
##
## mind that plugins may also affect menu
##########################################

#response.menu+=[
#    (T('Edit'), False, URL('admin', 'default', 'design/%s' % request.application),
#     [
#            (T('Controller'), False, 
#             URL('admin', 'default', 'edit/%s/controllers/%s.py' \
#                     % (request.application,request.controller=='appadmin' and
#                        'default' or request.controller))), 
#            (T('View'), False, 
#             URL('admin', 'default', 'edit/%s/views/%s' \
#                     % (request.application,response.view))),
#            (T('Layout'), False, 
#             URL('admin', 'default', 'edit/%s/views/layout.html' \
#                     % request.application)),
#            (T('Stylesheet'), False, 
#             URL('admin', 'default', 'edit/%s/static/base.css' \
#                     % request.application)),
#            (T('DB Model'), False, 
#             URL('admin', 'default', 'edit/%s/models/db.py' \
#                     % request.application)),
#            (T('Menu Model'), False, 
#             URL('admin', 'default', 'edit/%s/models/menu.py' \
#                     % request.application)),
#            (T('Database'), False, 
#             URL(request.application, 'appadmin', 'index')),
#
#            (T('Aggiungi libro'), False, 
#             URL(request.application, 'appadmin/insert/db/biblioteca', 'index')),
#
#            ]
#   ),
#  ]
