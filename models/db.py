# -*- coding: utf-8 -*- 

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
#########################################################################

webgrid = local_import('webgrid')


db = DAL('mysql://alcaffe:cafiero@127.0.0.1:3306/alcaffe')       # connect to db
#session.connect(request, response, db = db) # and store sessions and tickets
## if no need for session
# session.forget()

db.define_table('biblioteca',
    SQLField('autore', requires=IS_NOT_EMPTY()),
    SQLField('titolo', requires=IS_NOT_EMPTY()),
    SQLField('anno'),
    SQLField('editore'),
    SQLField('tema'),
    SQLField('inserti'),
    SQLField('prestito'),
    SQLField('utente'),
    SQLField('data_prestito', type='date', requires=IS_DATE()),
    SQLField('eliminato'),
    SQLField('vendita'),
    SQLField('prezzo'),
    SQLField('copie'),
    migrate=False)
    


db.define_table('video',
    SQLField('numero', requires=IS_NOT_EMPTY()),
    SQLField('regista'),
    SQLField('titolo', requires=IS_NOT_EMPTY()),
    SQLField('anno'),
    SQLField('paese'),
    SQLField('tema'),
    SQLField('tipo', requires=IS_IN_SET(['DVD', 'divx', 'VHS'])),
    SQLField('prestito'),
    SQLField('utente'),
    SQLField('data_prestito', type='date', requires=IS_DATE()),
    fake_migrate=True,
    migrate=False)
    
db.define_table('statistiche',
    SQLField('titolo'),
    SQLField('data', type='date', requires=IS_DATE()),
    SQLField('tipo'),
    fake_migrate=True,
    migrate=False)



#########################################################################
## Here is sample code if you need for 
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import *
mail = Mail()                                  # mailer
auth = Auth(globals(),db)                      # authentication/authorization
crud = Crud(globals(),db)                      # for CRUD helpers using auth
service = Service(globals())                   # for json, xml, jsonrpc, xmlrpc, amfrpc

mail.settings.server = 'logging' or 'localhost:25'  # your SMTP server
mail.settings.sender = 'tirasassi@canaglie.net'         # your email
mail.settings.login = None # 'username:password'      # your credentials or None

auth.settings.hmac_key = 'sha512:5e9e93c6-3e33-4878-8d60-6edc75a10b49'   # before define_tables()
auth.define_tables(username=True)                           # creates all needed tables
auth.settings.mailer = mail                    # for user email verification
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.messages.verify_email = 'Click on the link http://'+request.env.http_host+URL(r=request,c='default',f='user',args=['verify_email'])+'/%(key)s to verify your email'
auth.settings.reset_password_requires_verification = True
auth.messages.reset_password = 'Click on the link http://'+request.env.http_host+URL(r=request,c='default',f='user',args=['reset_password'])+'/%(key)s to reset your password'

# disable further registrations
auth.settings.actions_disabled.append('register')

#########################################################################
## If you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, uncomment and customize following
# from gluon.contrib.login_methods.rpx_account import RPXAccount
# auth.settings.actions_disabled=['register','change_password','request_reset_password']
# auth.settings.login_form = RPXAccount(request, api_key='...',domain='...',
#    url = "http://localhost:8000/%s/default/user/login" % request.application)
## other login methods are in gluon/contrib/login_methods
#########################################################################

crud.settings.auth = None                      # =auth to enforce authorization on crud

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################
