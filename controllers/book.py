# coding: utf8
import datetime

def list():
    link = request.args[0]
    op = request.args[1] # 0: full list, 1: lent, 2: stesso autore, 4: eliminati
    if (op == '1'):
        books = db(db.biblioteca.prestito==int(request.args[2])).select(orderby=db.biblioteca.autore|db.biblioteca.titolo)
        if (request.args[2] == '1'):
            response.flash = T('Libri in prestito')
            titolo = 'Libri in prestito'
        else:
            response.flash = T('Libri disponibili')
            titolo = 'Libri disponibili'
    elif (op == '2'):
        titolo = request.vars.autore
        books = db((db.biblioteca.autore==request.vars.autore)).select(db.biblioteca.titolo, db.biblioteca.id, db.biblioteca.autore, orderby=db.biblioteca.titolo)
    elif (op =='3'):
        titolo = 'Lista libri in vendita'
        books = db(db.biblioteca.vendita==1).select(orderby=db.biblioteca.autore|db.biblioteca.titolo)
        response.view = 'book/list_in_vendita.html'
    # lista dei libri eliminati
    elif (op == '4'):
        titolo = 'Lista libri eliminati'
        books = db(db.biblioteca.eliminato==1).select(orderby=db.biblioteca.autore)
    else:
        response.flash = T('Lista completa')
        books = db((db.biblioteca.id>0) & (db.biblioteca.eliminato==0) & (db.biblioteca.vendita==0)).select(orderby=db.biblioteca.autore|db.biblioteca.titolo)
        titolo = 'Lista completa'

    return dict(title=titolo, books=books)

@auth.requires(auth.user_id==1, requires_login=True)
def show():
    "mostra il singolo libro"
    T.lazy = False
    book = db.biblioteca[request.args[0]]
    if not book:
        redirect(URL(r=request, f='index'))
    table = [ ['Locazione' + ':', "al caffè"] ]
    if book.autore != '':
        table.append(['Autore' + ':', A(book.autore, _href=URL(c='book', r=request, f='list', args=['show', '2'], vars={ 'autore': book.autore}))])
    if book.editore != '':
        table.append(['Editore' + ':', book.editore])
    if book.anno != '':
        table.append(['Anno' + ':', book.anno])
    if book.tema != '':
        table.append(['Argomento' + ':', book.tema])
    if book.prestito == 1:
        rent = ''
        for row in db(db.biblioteca.id == request.args[0]).select():
            rent += row.utente
            rent += ' - ' + str(row.data_prestito)
        table.append(['Libro attualmente in prestito' + ':', rent])
        table.append([A('Restituzione', _href=URL(c='book', r=request, f='back', args=book.id)), ''])
    elif book.vendita == 1:
        for row in db(db.biblioteca.id == request.args[0]).select():
            table.append(['LIBRO IN VENDITA'])
    else:
        table.append([A('Dare in prestito', _href=URL(c='book', r=request, f='lend', args=book.id)), ''])
    table.append([A('Modificare i dati del libro', _href=URL(c='book', r=request, f='mod', args=book.id)), ''])
    table = TABLE(*[TR(*rows) for rows in table])
    T.lazy = True
    return dict(title=book.titolo, table=table)

@auth.requires(auth.user_id==1, requires_login=True)
def list_to_discard():
    titolo = 'Cliccare per definire un libro come eliminato (non più presente nella biblioteca)'
    books = db(db.biblioteca.eliminato==0).select(orderby=db.biblioteca.autore|db.biblioteca.titolo)
    return dict(title=titolo, books=books)

@auth.requires(auth.user_id==1, requires_login=True)
def lend():
    T.lazy = False
    title_options = [OPTION(book.titolo,_value=book.id) for book in db((db.biblioteca.prestito=="0")).select(orderby=db.biblioteca.titolo)]
    form = FORM(TABLE(
	TR(T('Titolo') + ': ', SELECT(*title_options, **dict(_name="id"))),
	TR(T('Utente') + ': ', INPUT(_name='name', _id='name', requires=IS_NOT_EMPTY(error_message=T('Missing data') + '!'))),
        #TR(T('Data') + ': ', INPUT(_name='date', _class='date', _id='date')), 
        TR("", INPUT(_type="submit", _value=T('Conferma prestito')))))
    form.vars.date = datetime.date.today()
    if len(request.args):
        form.vars.id = request.args[0]
    if form.accepts(request.vars, session):
        session.flash = T('Libro prestato')
        db(db.biblioteca.id==form.vars.id).update(prestito="1")
        db(db.biblioteca.id==form.vars.id).update(utente=form.vars.name, data_prestito=form.vars.date)
	#db(db.statistiche.insert(titolo=db.biblioteca.titolo, data=form.vars.date, tipo="libro"))
        redirect(URL(r=request, f='list/show/0'))
    elif form.errors:
        response.flash = T('Wrong data')
    T.lazy = True
    return dict(title=T('Pagina prestito libri'), form=form)

@auth.requires(auth.user_id==1, requires_login=True)
def back():
    T.lazy = False
    title_options = [OPTION(book.titolo,_value=book.id) for book in db((db.biblioteca.prestito=="1")).select(orderby=db.biblioteca.titolo)]
    form = FORM(TABLE(
        TR(T('Titolo') + ': ', SELECT(*title_options, **dict(_name="id"))),
        TR("", INPUT(_type="submit",_value=T('Restituzione'))),
        TR("", "Clicca sul pulsante per confermare la restituzione")))

    if len(request.args):
        form.vars.id = request.args[0]
    if form.accepts(request.vars, session):
        session.flash = T('Il libro e\' stato restituito')
        db(db.biblioteca.id==form.vars.id).update(prestito="0")
        db(db.biblioteca.id==form.vars.id).update(utente="", data_prestito="")
        redirect(URL(r=request, f='back'))
    elif form.errors:
        response.flash = T('Wrong data')
    T.lazy = True
    return dict(title=T('Pagina restituzione libri'), form=form)

@auth.requires(auth.user_id==1, requires_login=True)
def scartare():
    T.lazy = False
    book = db.biblioteca[request.args[0]]
    titolo = book.titolo
    autore = book.autore
    id = book.id
    db(db.biblioteca.id==id).update(eliminato="1")
    T.lazy = True

@auth.requires(auth.user_id==1, requires_login=True)
def add():
    T.lazy = False
    form = SQLFORM(db.biblioteca, fields = ['autore', 'titolo', 'anno', 'editore', 'tema', 'inserti'], labels = { 'autore': T('Autore'), 'titolo': T('Titolo'), 'Anno': T('Anno'), 'editore': T('Editore'), 'tema': T('Tema'), 'inserti': T('Inserti')}, submit_button = T('Aggiungi'))
    if form.accepts(request.vars, session):
        response.flash = T('Libro aggiunto al database')
    elif form.errors:
        response.flash = T('Wrong data')
    T.lazy = True
    return dict(title=T('Pagina aggiunta libri'), form=form)

@auth.requires(auth.user_id==1, requires_login=True)
def add_in_vendita():
    T.lazy = False
    form = SQLFORM(db.biblioteca, fields = ['autore', 'titolo', 'anno', 'editore', 'tema', 'inserti', 'prezzo', 'copie'], labels = { 'autore': T('Autore'), 'titolo': T('Titolo'), 'Anno': T('Anno'), 'editore': T('Editore'), 'tema': T('Tema'), 'inserti': T('Inserti'), 'prezzo': T('Prezzo'), 'copie': T('Copie')}, submit_button = T('Aggiungi'))
    form.vars.vendita = '1'
    if form.accepts(request.vars, session):
        response.flash = T('Libro aggiunto al database')
    elif form.errors:
        response.flash = T('Wrong data')
    T.lazy = True
    return dict(title=T('Pagina aggiunta libri in vendita'), form=form)

@auth.requires(auth.user_id==1, requires_login=True)
def mod():
    T.lazy = False
    book = db.biblioteca[request.args[0]]
    form = SQLFORM(db.biblioteca, book, fields = ['autore', 'titolo', 'anno', 'editore', 'tema', 'prestito', 'vendita', 'prezzo', 'copie', 'eliminato'])
    if form.accepts(request.vars, session):
        response.flash = T('Libro modificato')
    elif form.errors:
        response.flash = T('Wrong data')
    T.lazy = True
    return dict(title=T('Pagina modifica libri'), form=form)

def search():
    T.lazy = False
    ajax = "ajax('bg_search', ['titolo', 'autore', 'editore', 'tema'], 'target');"
    form = FORM(TABLE(
        TR(T('Titolo') + ': ', INPUT(_name='titolo', _id='titolo', _onchange=ajax)),
        TR(T('Autore') + ':', INPUT(_name='autore', _id='autore', _onchange=ajax)),
        TR(T('Editore') + ':', INPUT(_name='editore', _id='editore', _onchange=ajax)),
        TR(T('Tema') + ': ', INPUT(_name='tema', _id='tema', _onchange=ajax)),
        TR('', INPUT(_type='submit', _value=T('Trova')))), _onsubmit='return false;')
    T.lazy = True
    return dict(title=T('Ricerca'), form=form, target_div=DIV(_id='target'))

def bg_search():
    titolo = '%' + request.vars.titolo + '%'
    autore = '%' + request.vars.autore + '%'
    editore = '%' + request.vars.editore + '%'
    tema = '%' + request.vars.tema + '%'
    books = db(db.biblioteca.titolo.like(titolo) & db.biblioteca.autore.like(autore) & db.biblioteca.editore.like(editore) & db.biblioteca.tema.like(tema)).select(orderby=db.biblioteca.titolo)
    items = [A(row.autore, " - " , row.titolo, _href=URL(c='book', r=request, f='show', args=row.id)) for row in books]
    return UL(*items).xml()
