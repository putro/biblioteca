# coding: utf8


def index():
    grid = webgrid.WebGrid(crud)
    grid.crud_function = 'data'
    grid.datasource = db(db.biblioteca.id>0)
    grid.pagesize = 30
    grid.fields = ['biblioteca.id','biblioteca.autore','biblioteca.titolo','biblioteca.editore']
    grid.filters = ['biblioteca.autore','biblioteca.titolo']
    grid.action_links = ['view','edit']
    grid.action_headers = ['view','edit']

    grid.filter_query = lambda f, v: f >= v
    return dict(grid=grid())

def data():
    return dict(form=crud())
