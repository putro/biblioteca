# coding: utf8

def list():
    link = request.args[0]
    op = request.args[1] # 0: full list, 1: lent, 2: stesso regista
    if (op == '1'):
        videos = db(db.video.prestito==int(request.args[2])).select(orderby=db.video.regista|db.video.titolo)
        if (request.args[2] == '1'):
            response.flash = T('Video in prestito')
            titolo = 'Video in prestito'
        else:
            response.flash = T('Video disponibili')
            titolo = 'Video disponibili'
    elif (op == '2'):
        titolo = request.vars.regista
        videos = db((db.video.regista==request.vars.regista)).select(db.video.titolo, db.video.regista, db.video.id, orderby=db.video.titolo)

    else:
        response.flash = T('Lista completa')
        videos = db(db.video.id>0).select(orderby=db.video.regista|db.video.titolo)
        titolo = 'Lista completa'

    return dict(title=titolo, videos=videos)

def show():
    "mostra il singolo video"
    T.lazy = False
    video = db.video[request.args[0]]
    if not video:
        redirect(URL(r=request, f='index'))
    table = [ ['Locazione' + ':', "al caffè"] ]
    if video.numero != '':
	table.append(['Numero' + ':', video.numero])
    if video.regista != '':
        table.append(['Regista' + ':', A(video.regista, _href=URL(c='video', r=request, f='list', args=['show', '2'], vars={ 'regista': video.regista}))])
    if video.tipo != '':
        table.append(['Tipo' + ':', video.tipo])
    if video.anno != '':
        table.append(['Anno' + ':', video.anno])
    if video.tema != '':
        table.append(['Argomento' + ':', video.tema])
    if video.prestito == 1:
        rent = ''
        for row in db(db.video.id == request.args[0]).select():
            rent += row.utente
            rent += ' - ' + str(row.data_prestito)
        table.append(['In prestito' + ':', rent])
        table.append([A('Restituzione', _href=URL(c='video', r=request, f='back', args=video.id)), ''])
    else:
        table.append([A('Dare in prestito', _href=URL(c='video', r=request, f='lend', args=video.id)), ''])
    table = TABLE(*[TR(*rows) for rows in table])
    T.lazy = True
    return dict(title=video.titolo, table=table)

def lend():
    T.lazy = False
    title_options = [OPTION(video.titolo,_value=video.id) for video in db((db.video.prestito=="0")).select(orderby=db.video.titolo)]
    form = FORM(TABLE(
	TR(T('Titolo') + ': ', SELECT(*title_options, **dict(_name="id"))),
        TR(T('Utente') + ': ', INPUT(_name='name', _id='name', requires=IS_NOT_EMPTY(error_message=T('Missing data') + '!'))),
        TR(T('Data') + ': ', INPUT(_name='date', _class='date', _id='date')), 
        TR("", INPUT(_type="submit", _value=T('Conferma prestito')))))
    if len(request.args):
        form.vars.id = request.args[0]
    if form.accepts(request.vars, session):
        session.flash = T('Video prestato')
        db(db.video.id==form.vars.id).update(prestito="1")
        db(db.video.id==form.vars.id).update(utente=form.vars.name, data_prestito=form.vars.date)
        redirect(URL(r=request, f='list/show/0'))
    elif form.errors:
        response.flash = T('Wrong data')
    T.lazy = True
    return dict(title='Pagina prestiti video', form=form)


def back():
    T.lazy = False
    title_options = [OPTION(video.titolo,_value=video.id) for video in db((db.video.prestito=="1")).select(orderby=db.video.titolo)]
    form = FORM(TABLE(
        TR(T('Titolo') + ': ', SELECT(*title_options, **dict(_name="id"))),
        TR("", INPUT(_type="submit",_value=T('Restituzione'))),
	TR("", "Clicca sul pulsante per confermare la restituzione")))
    if len(request.args):
        form.vars.id = request.args[0]
    if form.accepts(request.vars, session):
        session.flash = 'Il video e\' stato restituito'
        db(db.video.id==form.vars.id).update(prestito="0")
        db(db.video.id==form.vars.id).update(utente="", data_prestito="")
        redirect(URL(r=request, f='back'))
	# dopo aver restituito il video torna a visualizzare la lista completa
    elif form.errors:
        response.flash = T('Wrong data')
    T.lazy = True
    return dict(title='Pagina restituzione video', form=form)

def add():
    T.lazy = False
    form = SQLFORM(db.video, fields = ['numero', 'titolo', 'regista', 'anno', 'paese', 'tema', 'tipo'], labels = { 'numero': T('Numero'), 'titolo': T('Titolo'), 'regista': T('Regista'),'anno': T('Anno'), 'paese': T('Paese'), 'tema': T('Tema'), 'tipo': T('Tipo')}, submit_button = T('Aggiungi'))
    if form.accepts(request.vars, session):
        response.flash = T('Video aggiunto al database')
    elif form.errors:
        response.flash = T('Wrong data')
    T.lazy = True
    return dict(title='Pagina aggiunta video', form=form)


def search():
    T.lazy = False
    ajax = "ajax('bg_search', ['titolo', 'regista', 'tema', 'tipo'], 'target');"
    form = FORM(TABLE(
        TR(T('Titolo') + ': ', INPUT(_name='titolo', _id='titolo', _onchange=ajax)),
        TR(T('Regista') + ':', INPUT(_name='regista', _id='regista', _onchange=ajax)),
        TR(T('Tipo') + ':', INPUT(_name='tipo', _id='tipo', _onchange=ajax)),
        TR(T('Tema') + ': ', INPUT(_name='tema', _id='tema', _onchange=ajax)),
        TR('', INPUT(_type='submit', _value=T('Trova')))), _onsubmit='return false;')
    T.lazy = True
    return dict(title=T('Ricerca'), form=form, target_div=DIV(_id='target'))


def bg_search():
    titolo = '%' + request.vars.titolo + '%'
    regista = '%' + request.vars.regista + '%'
    tipo = '%' + request.vars.tipo + '%'
    tema = '%' + request.vars.tema + '%'
    #books = db(db.biblioteca.titolo.like(titolo)).select(orderby=db.biblioteca.titolo)
    videos = db(db.video.titolo.like(titolo) & db.video.regista.like(regista) & db.video.tipo.like(tipo) & db.video.tema.like(tema)).select(orderby=db.video.titolo)
    items = [A(row.regista, " - " , row.titolo, _href=URL(c='video', r=request, f='show', args=row.id)) for row in videos]
    return UL(*items).xml()
