# -*- coding: utf-8 -*- 

#########################################################################
## This is a samples controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################  

def index():
        dvds = db((db.video.id > 0)).count()
        lent_dvds = db(db.video.prestito=="1").count()
        books = db((db.biblioteca.id > 0)).count()
        lent_books = db(db.biblioteca.prestito=="1").count()
        response.flash=T("Lista delle statistiche")
        return dict(film=dvds, film_in_prestito=lent_dvds, libri=books, libri_in_prestito=lent_books)


def quick_search():
    form=FORM(TABLE(
                TR([INPUT(_type='radio', _id='title', _name='option'), T('Title')], ''),
                TR([INPUT(_type='radio', _id='store', _name='option'), T('Store id')], INPUT(_type='hidden', _id='option')),
                TR(INPUT(_id='keyword'), INPUT(_type='submit', _value=T('Find')))),
                _onsubmit='start(); return false;', _id='quick_search')
    return dict(title=T('Quick search'), form=form, target_div_dvd=DIV(_id='target_dvd'), target_div_book=DIV(_id='target_book'))

def bg_quick_dvd():
    if (request.vars.option == '1'):
        pattern = '%' + request.vars.keyword + '%'
        dvds = db(db.video.titolo.like(pattern)).select(orderby=db.video.titolo)
    else:
        #qui' e' da sistemare
        dvds = db(db.video.store==request.vars.keyword).select(orderby=db.video.titolo)
    items = [A(row.title, _href=URL(c='dvd', r=request, f='show', args=row.id)) for row in dvds]
    return UL(*items).xml()
 
def bg_quick_book():
    if (request.vars.option == '1'):
        pattern = '%' + request.vars.keyword + '%'
        books = db(db.biblioteca.titolo.like(pattern)).select(orderby=db.biblioteca.titolo)
    else:
         #qui' e' da sistemare
        books = db((db.books.user==user_id) & (db.books.store==request.vars.keyword)).select(orderby=db.books.title)
    items = [A(row.title, _href=URL(c='book', r=request, f='show', args=row.id)) for row in books]
    return UL(*items).xml()





def about():
    return dict(title=T('About'))

#autocomplete
def get_items():
    itms = []
    if request.vars.q and \
       request.vars.table and \
       request.vars.field:
        q = request.vars.q
        f = request.vars.field
        t = request.vars.table
        fld = db[t][f]
        rows = db(fld.upper().like(q.upper()+"%")).select(fld,distinct=True) 
        itms = [str(t[f]) for t in rows] 
    return '\n'.join(itms)





def user():
    """
    exposes:
    http://..../[app]/default/user/login 
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request,db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    session.forget()
    return service()
