# -*- coding: utf-8 -*-

def ebooks():
    inputfiles = _processFile("/mnt/hd/backup/libri")
    table = []
    t = {}
    for file in inputfiles:
        try:
            autore = file.split("-")[0].split("/")[-1]
            titolo = file.split("-", 1)[1].split(".")[0]
            titolot = titolo.replace("_", " ")
            ext = file.split(".")[-1]
            tema = file.split("/")[-2]
            downlink = file.split("/")[-3] + "/" + tema + "/" + file.split("/")[-1]
            #fileName, fileExtension = os.path.splitext(file)
            table.append([autore, titolot, ext, tema, file, downlink])
        except:
            continue
            #return dict(table="Errore sul file %s  probabilmente non ha un nome conforme (Autore-Titolo.pdf)" % file)
    table = _sort_table(table, col=int(request.args[0]))
    return dict(table=table)


def _processFile(dir):
    # restituisce la lista dei files presenti in una dir (comprese le sottodir)
    # il valore ritornato e' una lista
    import os
    files = []
    for dirname, dirnames, filenames in os.walk(dir):
        # print path to all filenames.
        for filename in filenames:
            files.append(os.path.join(dirname, filename))
    return sorted(files)

import operator

def _sort_table(table, col=0):
    return sorted(table, key=operator.itemgetter(col))
